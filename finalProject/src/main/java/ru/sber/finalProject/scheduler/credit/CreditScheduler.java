package ru.sber.finalProject.scheduler.credit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.sber.finalProject.entity.credit.Card;
import ru.sber.finalProject.entity.credit.Credit;
import ru.sber.finalProject.repository.credit.CardRepository;
import ru.sber.finalProject.repository.credit.CreditRepository;

import java.util.Date;
import java.util.List;

@Component
public class CreditScheduler {

    private final CreditRepository creditRepository;
    private final CardRepository cardRepository;

    @Autowired
    public CreditScheduler(CreditRepository creditRepository, CardRepository cardRepository) {
        this.creditRepository = creditRepository;
        this.cardRepository = cardRepository;
    }


    //Проверка срока кредита на просрочку каждый день
    @Scheduled(fixedDelay = 86400000)
    public void checkDateCredit() {
        List<Credit> creditList = creditRepository.findAll();
        Date date = new Date();
        for (Credit credit : creditList) {
            if (credit.getDeadlineClosable().getTime() < date.getTime()) {
                if (credit.getPenaltiesPercent() <= 40) {
                    credit.setPenaltiesPercent(credit.getPenaltiesPercent() + 1);
                }
                credit.setSum(credit.getSum() + ((credit.getSum() * credit.getPenaltiesPercent()) / 100));
                creditRepository.save(credit);
            }
        }
    }

    //Закрытие кредитной карты, если истёк срок её службы
    @Scheduled(fixedDelay = 1800000)
    public void checkDateCreditCard() {
        List<Card> cardList = cardRepository.findAll();
        Date date = new Date();
        for (Card card : cardList) {
            if (card.getDeadline().getTime() <= date.getTime() && card.getCredits().isEmpty()) {
                cardRepository.delete(card);
            }
        }
    }

}
