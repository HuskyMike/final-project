package ru.sber.finalProject.scheduler.deposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.sber.finalProject.entity.deposit.Deposit;
import ru.sber.finalProject.repository.deposit.DepositRepository;

import java.util.Date;
import java.util.List;

@Component
public class DepositScheduler {

    private final DepositRepository depositRepository;

    @Autowired
    public DepositScheduler(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    //Расчет выплат по проценту вложения
    @Scheduled(fixedDelay = 86400000)
    public void calculation() {
        List<Deposit> depositList = depositRepository.findAll();
        for (Deposit deposit : depositList) {
            long moth = 2592000000l;
            Date dateBegin = deposit.getDateBegin();
            dateBegin.setTime(dateBegin.getTime() + moth);
            Date dateNow = new Date();
            if (dateBegin.getTime() <= dateNow.getTime()) {
                deposit.setBalance(deposit.getBalance() + (deposit.getMin() * deposit.getPercent()));
                deposit.setDateBegin(dateNow);
                depositRepository.save(deposit);
            }
        }
    }

    @Scheduled(fixedDelay = 86400000)
    public void extensionDeadline() {
        List<Deposit> depositList = depositRepository.findAll();
        Date dateNow = new Date();
        for (Deposit deposit : depositList) {
            if (deposit.getDuration().getTime() <= dateNow.getTime()) {
                Date dateExtension = deposit.getDuration();
                dateExtension.setTime(dateExtension.getTime() + 2592000000l);
                deposit.setDuration(dateExtension);
                depositRepository.save(deposit);
            }
        }
    }


}
