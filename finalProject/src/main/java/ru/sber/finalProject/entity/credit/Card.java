package ru.sber.finalProject.entity.credit;

import ru.sber.finalProject.enums.StatusCard;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "card")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String number; //номер карты

    private double score; //счёт

    @Column(name = "limit_card")
    private double limit; //лимит карты

    private Date deadline; //срок действия

    @Enumerated(EnumType.STRING)
    private StatusCard statusCard; // работоспособность

    private double percent; //процент

    private String formatDate; //читабельный формат даты


    @OneToMany(mappedBy = "card", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Credit> credits = new HashSet<>();


    public Card() {
    }

    public Card(String number, double score, double limit, Date deadline, StatusCard statusCard, double percent, Set<Credit> credits) {
        this.number = number;
        this.score = score;
        this.limit = limit;
        this.deadline = deadline;
        this.statusCard = statusCard;
        this.percent = percent;
        this.credits = credits;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        this.formatDate = sdf.format(deadline);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public StatusCard getStatusCard() {
        return statusCard;
    }

    public void setStatusCard(StatusCard statusCard) {
        this.statusCard = statusCard;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public Set<Credit> getCredits() {
        return credits;
    }

    public void setCredits(Set<Credit> credits) {
        this.credits = credits;
    }

    public String getFormatDate() {
        return formatDate;
    }

    public void setFormatDate(String formatDate) {
        this.formatDate = formatDate;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", score=" + score +
                ", limit=" + limit +
                ", deadline=" + deadline +
                ", statusCard=" + statusCard +
                ", percent=" + percent +
                ", credits=" + credits +
                '}';
    }
}
