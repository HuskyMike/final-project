package ru.sber.finalProject.entity.credit;

import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.enums.Status;

import javax.persistence.*;

//Заявка на открытие кредита
@Entity
@Table(name = "applications")
public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name_application")
    private String name; //Название

    @Enumerated(EnumType.STRING)
    private Currency currency;  //Валюта

    @Enumerated(EnumType.STRING)
    private Status status; //Статус

    public Application() {
    }

    public Application(String name, Currency currency, Status status) {
        this.name = name;
        this.currency = currency;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Application{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", currency=" + currency +
                ", status=" + status +
                '}';
    }
}
