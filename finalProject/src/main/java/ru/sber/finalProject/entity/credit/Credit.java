package ru.sber.finalProject.entity.credit;

import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.enums.Status;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "credits")
public class Credit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String number; //номер карты

    @Column(name = "name_credit")
    private String name; //название

    private double sum; //сумма

    @Column(name = "limit_sum")
    private double limit; //лимит

    private double percent; //процент

    @Enumerated(EnumType.STRING)
    private Currency currency; //валюта

    @Enumerated(EnumType.STRING)
    private Status status; //статус

    private double commission; //комиссия за валюту

    private double penaltiesPercent; //процент пени

    private Date deadlineClosable; //Срок закрытия кредита

    private String formatDate; //формат даты

    @ManyToOne
    @JoinColumn(name = "card_id", nullable = false)
    private Card card;


    public Credit() {
    }

    public Credit(String number, String name, double sum, double limit, double percent,
                  Currency currency, double penalties, Status status, Card card,
                  Date deadlineClosable) {
        this.number = number;
        this.name = name;
        this.sum = sum;
        this.limit = limit;
        this.percent = percent;
        this.currency = currency;
        this.penaltiesPercent = penalties;
        this.status = status;
        this.card = card;
        this.deadlineClosable = deadlineClosable;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        this.formatDate = sdf.format(deadlineClosable);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public double getCommission() {
        return commission;
    }

    public double getPenaltiesPercent() {
        return penaltiesPercent;
    }

    public void setPenaltiesPercent(double penaltiesPercent) {
        this.penaltiesPercent = penaltiesPercent;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public Date getDeadlineClosable() {
        return deadlineClosable;
    }

    public void setDeadlineClosable(Date deadlineClosable) {
        this.deadlineClosable = deadlineClosable;
    }

    public String getFormatDate() {
        return formatDate;
    }

    public void setFormatDate(String formatDate) {
        this.formatDate = formatDate;
    }

    @Override
    public String toString() {
        return "Credit{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", name='" + name + '\'' +
                ", sum=" + sum +
                ", limit=" + limit +
                ", percent=" + percent +
                ", currency=" + currency +
                ", status=" + status +
                ", commission=" + commission +
                ", penaltiesPercent=" + penaltiesPercent +
                ", deadlineClosable=" + deadlineClosable +
                ", card=" + card +
                '}';
    }
}
