package ru.sber.finalProject.entity.transfer;

import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.enums.Status;

import javax.persistence.*;

@Entity
@Table(name = "history_operation")
public class HistoryOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String numberSender; //номер счета или карты отправителя

    private String numberRecipient; //номер счета или карты принимающей стороны

    private double sum; //сумма перевода

    @Enumerated(EnumType.STRING)
    private Status status; // Статус операции

    @Enumerated(EnumType.STRING)
    private Currency currency; //Валюта

    public HistoryOperation() {
    }

    public HistoryOperation(String numberSender, String numberRecipient, double sum, Status status, Currency currency) {
        this.numberSender = numberSender;
        this.numberRecipient = numberRecipient;
        this.sum = sum;
        this.status = status;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumberSender() {
        return numberSender;
    }

    public void setNumberSender(String numberSender) {
        this.numberSender = numberSender;
    }

    public String getNumberRecipient() {
        return numberRecipient;
    }

    public void setNumberRecipient(String numberRecipient) {
        this.numberRecipient = numberRecipient;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "HistoryOperation{" +
                "id=" + id +
                ", numberSender='" + numberSender + '\'' +
                ", numberRecipient='" + numberRecipient + '\'' +
                ", sum=" + sum +
                ", status=" + status +
                ", currency=" + currency +
                '}';
    }
}
