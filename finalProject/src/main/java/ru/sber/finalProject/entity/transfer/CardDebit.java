package ru.sber.finalProject.entity.transfer;

import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.enums.StatusCard;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
@Table(name = "card_debet")
public class CardDebit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String number; //номер карты

    private double balance; //баланс

    private Date deadline; //срок действия

    @Enumerated(EnumType.STRING)
    private StatusCard statusCard; // работоспособность

    @Enumerated(EnumType.STRING)
    private Currency currency; // валюта

    private String formatDate; // формат даты

    public CardDebit() {
    }

    public CardDebit(String number, double balance, Date deadline, StatusCard statusCard, Currency currency) {
        this.number = number;
        this.balance = balance;
        this.deadline = deadline;
        this.statusCard = statusCard;
        this.currency = currency;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        this.formatDate = sdf.format(deadline);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public StatusCard getStatusCard() {
        return statusCard;
    }

    public void setStatusCard(StatusCard statusCard) {
        this.statusCard = statusCard;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getFormatDate() {
        return formatDate;
    }

    public void setFormatDate(String formatDate) {
        this.formatDate = formatDate;
    }

    @Override
    public String toString() {
        return "CardDebit{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", balance=" + balance +
                ", deadline=" + deadline +
                ", statusCard=" + statusCard +
                ", currency=" + currency +
                '}';
    }
}
