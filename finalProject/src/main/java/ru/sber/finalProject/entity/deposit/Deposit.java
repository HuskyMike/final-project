package ru.sber.finalProject.entity.deposit;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Entity
public class Deposit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name_deposit")
    private String name; //название вложения

    private double balance; // баланс

    @Column(name = "minimum")
    private double min; //минимальное вложение

    @Column(name = "maximum")
    private double max; // максимальное вложение

    private Date duration; //длительность вклада

    private Date dateBegin; // Дата открытия вклада

    private double percent; // процентная стака вклада

    private boolean investing; //пополняемость

    private boolean closable; //возможность закрыть вклад

    private boolean capitalization; // капитализируемый

    private boolean possibilityTake; // возможность снятия

    private String formatDate; // формат даты

    public Deposit() {
    }

    public Deposit(String name, double balance, double min, double max,
                   Date duration, double percent, boolean investing, boolean closable,
                   boolean capitalization, boolean possibilityTake) {
        this.name = name;
        this.balance = balance;
        this.min = min;
        this.max = max;
        this.duration = duration;
        this.percent = percent;
        this.investing = investing;
        this.closable = closable;
        this.capitalization = capitalization;
        this.possibilityTake = possibilityTake;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        this.formatDate = sdf.format(duration);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public Date getDuration() {
        return duration;
    }

    public void setDuration(Date duration) {
        this.duration = duration;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public boolean isInvesting() {
        return investing;
    }

    public void setInvesting(boolean investing) {
        this.investing = investing;
    }

    public boolean isClosable() {
        return closable;
    }

    public void setClosable(boolean closable) {
        this.closable = closable;
    }

    public boolean isCapitalization() {
        return capitalization;
    }

    public void setCapitalization(boolean capitalization) {
        this.capitalization = capitalization;
    }

    public boolean isPossibilityTake() {
        return possibilityTake;
    }

    public void setPossibilityTake(boolean possibilityTake) {
        this.possibilityTake = possibilityTake;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public String getFormatDate() {
        return formatDate;
    }

    public void setFormatDate(String formatDate) {
        this.formatDate = formatDate;
    }

    @Override
    public String toString() {
        return "Deposit{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", min=" + min +
                ", max=" + max +
                ", duration=" + duration +
                ", dateBegin=" + dateBegin +
                ", percent=" + percent +
                ", investing=" + investing +
                ", closable=" + closable +
                ", capitalization=" + capitalization +
                ", possibilityTake=" + possibilityTake +
                '}';
    }
}
