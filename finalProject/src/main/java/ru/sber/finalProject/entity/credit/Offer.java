package ru.sber.finalProject.entity.credit;

import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.enums.Status;

import javax.persistence.*;

@Entity
@Table(name = "offers")
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name_application")
    private String name; //Название

    @Enumerated(EnumType.STRING)
    private Currency currency;  //Валюта

    private double percent; //предлагаемый процент

    @Column(name = "limit_offer")
    private double limit; // предлагаемый лимит

    @Enumerated(EnumType.STRING)
    private Status status; // Статус заявки

    public Offer() {
    }

    public Offer(String name, Currency currency, double percent, double limit, Status status) {
        this.name = name;
        this.currency = currency;
        this.percent = percent;
        this.limit = limit;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public double getLimit() {
        return limit;
    }

    public void setLimit(double limit) {
        this.limit = limit;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Offer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", currency=" + currency +
                ", percent=" + percent +
                ", limit=" + limit +
                ", status=" + status +
                '}';
    }
}
