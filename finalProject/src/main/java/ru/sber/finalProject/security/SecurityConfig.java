package ru.sber.finalProject.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.sber.finalProject.entity.security.Authorization;
import ru.sber.finalProject.repository.authorization.AuthorizationRepository;

import java.util.List;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthorizationRepository authorizationRepository;

    @Autowired
    public SecurityConfig(AuthorizationRepository authorizationRepository) {
        this.authorizationRepository = authorizationRepository;
    }

    @Bean("authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        List<Authorization> authorizationList = authorizationRepository.findAll();
        for (Authorization authorization : authorizationList) {
            if (authorization.getRoles().equals("CLIENT")) {
                auth.inMemoryAuthentication()
                        .withUser(authorization.getLogin())
                        .password(authorization.getPassword())
                        .roles(authorization.getRoles());
            }
            if (authorization.getRoles().equals("OPERATOR")) {
                auth.inMemoryAuthentication()
                        .withUser(authorization.getLogin())
                        .password(authorization.getPassword())
                        .roles(authorization.getRoles());
            }
        }
    }


    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/operator/**").hasRole("OPERATOR")
                .antMatchers("/client/**", "/credit/**").hasRole("CLIENT")
                .and().formLogin().defaultSuccessUrl("/", false)
                .and()
                .csrf().disable();

    }


}
