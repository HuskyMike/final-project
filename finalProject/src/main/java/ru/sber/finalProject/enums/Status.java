package ru.sber.finalProject.enums;

public enum Status {
    Waiting, //Ожидание
    Rejected, //Отклонена
    Approved //Одобрена
}
