package ru.sber.finalProject.enums;

public enum Currency {
    RUB,
    EUR,
    USD
}
