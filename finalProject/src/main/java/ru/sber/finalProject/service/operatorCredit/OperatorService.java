package ru.sber.finalProject.service.operatorCredit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.finalProject.entity.credit.Application;
import ru.sber.finalProject.entity.credit.Credit;
import ru.sber.finalProject.entity.credit.Offer;
import ru.sber.finalProject.enums.Status;
import ru.sber.finalProject.repository.credit.ApplicationRepository;
import ru.sber.finalProject.repository.credit.CreditRepository;
import ru.sber.finalProject.repository.credit.OfferRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class OperatorService {

    private final ApplicationRepository applicationRepository;
    private final OfferRepository offerRepository;
    private final CreditRepository creditRepository;

    @Autowired
    public OperatorService(ApplicationRepository applicationRepository, OfferRepository offerRepository, CreditRepository creditRepository) {
        this.applicationRepository = applicationRepository;
        this.offerRepository = offerRepository;
        this.creditRepository = creditRepository;
    }


    //Список всех заявок клиента
    public Iterable<Application> applicationList() {
        Iterable<Application> applications = applicationRepository.findAll();
        return applications;
    }

    //Список предложений
    public Iterable<Offer> listOffer() {
        Iterable<Offer> offers = offerRepository.findAll();
        return offers;
    }

    //Информация о заявке
    public ArrayList<Application> applicationId(Long id) {
        Optional<Application> application = applicationRepository.findById(id);
        ArrayList<Application> res = new ArrayList<>();
        application.ifPresent(res::add);
        return res;
    }

    //Создание Предложения
    public void createOffer(long id, double limit, double percent) {
        Application application = applicationRepository.findById(id).get();
        Offer offer = new Offer(application.getName(), application.getCurrency(), percent,
                limit, Status.Approved);
        offerRepository.save(offer);
    }

    //Удаления заявки
    public void removeApplication(long id) {
        Application application = applicationRepository.findById(id).get();
        applicationRepository.delete(application);
    }

    //Сбор всех строк из таблицы кредита
    public Iterable<Credit> listCredit() {
        Iterable<Credit> credits = creditRepository.findAll();
        return credits;
    }

    //Информация по кредиту
    public ArrayList<Credit> credit(long id) {
        Optional<Credit> credit = creditRepository.findById(id);
        ArrayList<Credit> res = new ArrayList<>();
        credit.ifPresent(res::add);
        return res;
    }

    //Изменение комиссии у кредита
    public void updateCommission(long id, double commission) {
        Credit credit = creditRepository.findById(id).get();
        credit.setCommission(commission);
        creditRepository.save(credit);
    }

    //Изменение процент пени у кредита
    public void updatePenaltiesPercent(long id, double penaltiesPercent) {
        Credit credit = creditRepository.findById(id).get();
        credit.setPenaltiesPercent(penaltiesPercent);
        creditRepository.save(credit);
    }

}
