package ru.sber.finalProject.service.operatorDeposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.finalProject.entity.deposit.Deposit;
import ru.sber.finalProject.repository.deposit.DepositRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

@Service
public class OperatorDepositService {

    private final DepositRepository depositRepository;

    @Autowired
    public OperatorDepositService(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    //Отображать весь список вложений
    public Iterable<Deposit> deposits() {
        return depositRepository.findAll();
    }

    //Информация о депозите
    public ArrayList<Deposit> infoDeposit(long id) {
        Optional<Deposit> deposit = depositRepository.findById(id);
        ArrayList<Deposit> result = new ArrayList<>();
        deposit.ifPresent(result::add);
        return result;
    }

    //изменение вклада
    public void editDeposit(long id, String name, double min, double max, Date duration,
                            double percent, boolean investing, boolean closable,
                            boolean capitalization, boolean possibilityTake) {
        Deposit deposit = depositRepository.findById(id).get();
        deposit.setName(name);
        deposit.setMin(min);
        deposit.setMax(max);
        deposit.setDuration(duration);
        deposit.setInvesting(investing);
        deposit.setClosable(closable);
        deposit.setCapitalization(capitalization);
        deposit.setPossibilityTake(possibilityTake);
        deposit.setPercent(percent);
        depositRepository.save(deposit);
    }

}
