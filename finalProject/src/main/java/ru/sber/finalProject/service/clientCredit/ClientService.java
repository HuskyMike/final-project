package ru.sber.finalProject.service.clientCredit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.finalProject.entity.credit.Card;
import ru.sber.finalProject.entity.credit.Credit;
import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.enums.Status;
import ru.sber.finalProject.repository.credit.CardRepository;
import ru.sber.finalProject.repository.credit.CreditRepository;

import java.io.IOException;
import java.util.*;

@Service
public class ClientService {

    private final CardRepository cardRepository;
    private final CreditRepository creditRepository;

    @Autowired
    public ClientService(CardRepository cardRepository, CreditRepository creditRepository) {
        this.cardRepository = cardRepository;
        this.creditRepository = creditRepository;
    }

    //Список всех карт
    public Iterable<Card> listCards() {
        Iterable<Card> cards = cardRepository.findAll();
        return cards;
    }

    //Детальная информация о карте
    public ArrayList<Card> infoCard(Long id) {
        Optional<Card> card = cardRepository.findById(id);
        ArrayList<Card> res = new ArrayList<>();
        card.ifPresent(res::add);
        return res;
    }

    //Получение имени всех кредитов карты
    public Set<Credit> creditList(Long id) {
        Card card = cardRepository.findById(id).get();
        Set<Credit> credits = card.getCredits();
        return credits;
    }

    //Добавления кредита
    public void createCredit(String name, double sum, Currency currency,
                             Long id) {
        Card card = cardRepository.findById(id).get();
        Set<Credit> listCard = card.getCredits();
        boolean checkDate = true;
        double checkSum = 0;
        Date date = new Date();
        //Перевод суммы на рубли для проверки
        if (currency.equals(Currency.EUR)) {
            checkSum = sum * actualCurrency(Currency.EUR);
        }
        if (currency.equals(Currency.USD)) {
            checkSum = sum * actualCurrency(Currency.USD);
        }
        if (currency.equals(Currency.RUB)) {
            checkSum = sum;
        }

        //Проверка кредитов на просрочку
        for (Credit credit : listCard) {
            if (credit.getDeadlineClosable().getTime() < date.getTime()) {
                checkDate = false;
            }
        }

        if (checkDate) {
            //Проверка даты карты и кредита
            if (card.getLimit() >= checkSum && checkSum > 0) {
                double sumByPercent = sum + ((sum * card.getPercent()) / 100);
                Date deadlineClosable;
                Calendar calendar = new GregorianCalendar();
                calendar.add(Calendar.DATE, 30);
                deadlineClosable = calendar.getTime();
                if (card.getDeadline().getTime() < calendar.getTimeInMillis()) {
                    deadlineClosable = card.getDeadline();
                    Calendar checkDay = new GregorianCalendar();
                    checkDay.add(Calendar.DATE, 1);
                    if (deadlineClosable.getTime() < checkDay.getTimeInMillis()) {
                        deadlineClosable = checkDay.getTime();
                    }
                }
                Credit credit = new Credit(card.getNumber(), name, sumByPercent, card.getLimit(), card.getPercent()
                        , currency, 0, Status.Approved, card, deadlineClosable);
                credit.setCommission(10d);
                //Перевод на рубли
                if (currency.equals(Currency.EUR)) {
                    card.setScore(card.getScore() + (sum * actualCurrency(Currency.EUR))
                            - (((sum * actualCurrency(Currency.EUR))
                            * credit.getCommission()) / 100));
                }
                if (currency.equals(Currency.USD)) {
                    card.setScore(card.getScore() + (sum * actualCurrency(Currency.USD))
                            - (((sum * actualCurrency(Currency.USD))
                            * credit.getCommission()) / 100));
                }
                if (currency.equals(Currency.RUB)) {
                    card.setScore(card.getScore() + sum);
                }
                creditRepository.save(credit);
                card.setLimit(card.getLimit() - checkSum);
                cardRepository.save(card);
            } else {
                System.out.println("Невозможно взять кредит, так как меньше лимита");
            }
        } else {
            System.out.println("Просрочен один из кредитовб взять кредит невозможно");
        }
    }

    //Выдача одной карты по id
    public Card card(Long id) {
        return cardRepository.findById(id).get();
    }

    //Выдача кредита по id
    public Credit credit(Long id) {
        return creditRepository.findById(id).get();
    }

    //Погашение кредита
    public void repaymentCredit(Long id, double sum) {
        Credit credit = creditRepository.findById(id).get();
        double resSum = sum;
        if (credit.getCurrency().equals(Currency.EUR)) {
            resSum = (sum - ((sum * credit.getCommission()) / 100)) / actualCurrency(Currency.EUR);
        }
        if (credit.getCurrency().equals(Currency.USD)) {
            resSum = (sum - ((sum * credit.getCommission()) / 100)) / actualCurrency(Currency.USD);
        }
        if (credit.getSum() <= resSum) {
            creditRepository.delete(credit);
        } else {
            double score = credit.getSum() - resSum;
            credit.setSum(score);
            creditRepository.save(credit);
        }
    }

    //Получение актуального курса валют
    private double actualCurrency(Currency currency) {
        //USD
        if (currency.equals(Currency.USD)) {
            try {
                Document document = Jsoup.connect("https://www.banki.ru/products/currency/cash/samara/").get();
                Elements el = document.select("#widgetAkbars > div > div:nth-child(2) > div.table-flex__th." +
                        "table-flex__cell--width-x1.akbars-table__flex-basis.akbars-table__border." +
                        "akbars-table__border--no-border-bottom.font-size-xx-large > div");
                String[] arrayString = el.text().split(" ");
                return Double.parseDouble(arrayString[1].replace(",", "."));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //EUR
        if (currency.equals((Currency.EUR))) {
            try {
                Document document = Jsoup.connect("https://www.banki.ru/products/currency/cash/samara/").get();
                Elements el = document.select("#widgetAkbars > div > div:nth-child(3) > div.table-flex__th." +
                        "table-flex__cell--width-x1.akbars-table__flex-basis." +
                        "akbars-table__border.font-size-xx-large > div");
                String[] arrayString = el.text().split(" ");
                return Double.parseDouble(arrayString[1].replace(",", "."));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

}
