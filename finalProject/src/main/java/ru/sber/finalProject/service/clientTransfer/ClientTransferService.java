package ru.sber.finalProject.service.clientTransfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.finalProject.entity.transfer.CardDebit;
import ru.sber.finalProject.entity.transfer.HistoryOperation;
import ru.sber.finalProject.entity.transfer.Score;
import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.enums.Status;
import ru.sber.finalProject.enums.StatusCard;
import ru.sber.finalProject.repository.transfer.CardDebitRepository;
import ru.sber.finalProject.repository.transfer.HistoryOperationRepository;
import ru.sber.finalProject.repository.transfer.ScoreRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

@Service
public class ClientTransferService {

    private final ScoreRepository scoreRepository;
    private final CardDebitRepository cardDebitRepository;
    private final HistoryOperationRepository historyOperationRepository;

    @Autowired
    public ClientTransferService(ScoreRepository scoreRepository, CardDebitRepository cardDebitRepository,
                                 HistoryOperationRepository historyOperationRepository) {
        this.scoreRepository = scoreRepository;
        this.cardDebitRepository = cardDebitRepository;
        this.historyOperationRepository = historyOperationRepository;
    }

    //Создание счета
    public void createScore(double balance, Currency currency) {
        if (balance >= 0) {
            Score score = new Score(balance, currency);
            scoreRepository.save(score);
        } else {
            System.out.println("Отрицательный баланс создавать невозможно");
        }
    }

    //Список счетов
    public Iterable<Score> scores() {
        Iterable<Score> scores = scoreRepository.findAll();
        return scores;
    }

    //Информация о счете
    public ArrayList<Score> infoScore(long id) {
        Optional<Score> score = scoreRepository.findById(id);
        ArrayList<Score> res = new ArrayList<>();
        score.ifPresent(res::add);
        return res;
    }

    //Перевод денежных средств с одного счета на другой
    public void transferMoneyToScore(long id, long idScore, double sum) {
        Score scoreSender = scoreRepository.findById(id).get();
        Score scoreRecipient = scoreRepository.findById(idScore).get();
        if (scoreSender.getBalance() >= sum) {
            double balanceSender = scoreSender.getBalance() - sum;
            double balanceRecipient = scoreRecipient.getBalance() + sum;
            scoreSender.setBalance(balanceSender);
            scoreRecipient.setBalance(balanceRecipient);
            scoreRepository.save(scoreRecipient);
            scoreRepository.save(scoreSender);
            HistoryOperation historyOperation = new HistoryOperation(scoreSender.getId().toString(),
                    scoreRecipient.getId().toString(), sum,
                    Status.Approved, scoreSender.getCurrency());
            historyOperationRepository.save(historyOperation);
        } else {
            System.out.println("Недостаточно средств на счету");
        }
    }

    //Удаление счета
    public void deleteScore(long id) {
        scoreRepository.deleteById(id);
    }


    //Отображение всех дебетовых карт
    public Iterable<CardDebit> cardsDebit() {
        Iterable<CardDebit> cards = cardDebitRepository.findAll();
        return cards;
    }

    //Создание дебетовой карты
    public void createCardDebit(Currency currency) {
        Calendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DATE, 90);
        CardDebit cardDebit = new CardDebit(null, 0, calendar.getTime(), StatusCard.Connect, currency);
        cardDebitRepository.save(cardDebit);
        cardDebit.setNumber("5456555" + cardDebit.getId());
        cardDebitRepository.save(cardDebit);
    }

    //Получение подробной информации о карте
    public ArrayList<CardDebit> infoCardDebit(long id) {
        Optional<CardDebit> cardDebit = cardDebitRepository.findById(id);
        ArrayList<CardDebit> res = new ArrayList<>();
        cardDebit.ifPresent(res::add);
        return res;
    }

    //Удаление дебетовой карты
    public void deleteCardDebit(long id) {
        cardDebitRepository.deleteById(id);
    }

    //Денежные переводы с карты на карту
    public void transferCardToCard(long id, String numberCard, double sum) {
        CardDebit cardDebitSender = cardDebitRepository.findById(id).get();
        String codeBank = "5456555";
        String codeBankRecipient = numberCard.substring(0, 7);
        if (cardDebitSender.getBalance() >= sum) {
            if (codeBankRecipient.equals(codeBank)) {
                long idCard = Long.parseLong(numberCard.substring(7));
                CardDebit cardDebitRecipient = cardDebitRepository.findById(idCard).get();
                cardDebitSender.setBalance(cardDebitSender.getBalance() - sum);
                cardDebitRecipient.setBalance(cardDebitRecipient.getBalance() + sum);
                cardDebitRepository.save(cardDebitRecipient);
                HistoryOperation historyOperation = new HistoryOperation(cardDebitSender.getNumber(),
                        cardDebitRecipient.getNumber(), sum,
                        Status.Approved, cardDebitSender.getCurrency());
                historyOperationRepository.save(historyOperation);
            } else {
                int commission = 5;
                cardDebitSender.setBalance(cardDebitSender.getBalance() - ((sum * commission / 100) + sum));
                cardDebitRepository.save(cardDebitSender);
                HistoryOperation historyOperation = new HistoryOperation(cardDebitSender.getNumber(),
                        numberCard, sum,
                        Status.Waiting, cardDebitSender.getCurrency());
                historyOperationRepository.save(historyOperation);
            }
        } else {
            System.out.println("Недостаточно денежных средств");
        }

    }

    //Перевод с карты на счет
    public void transferCardToScore(long id, long numberScore, double sum) {
        CardDebit cardDebitSender = cardDebitRepository.findById(id).get();
        Score scoreRecipient = scoreRepository.findById(numberScore).get();
        if (cardDebitSender.getBalance() >= sum) {
            cardDebitSender.setBalance(cardDebitSender.getBalance() - sum);
            scoreRecipient.setBalance(scoreRecipient.getBalance() + sum);
            scoreRepository.save(scoreRecipient);
            cardDebitRepository.save(cardDebitSender);
            HistoryOperation historyOperation = new HistoryOperation(cardDebitSender.getNumber(),
                    scoreRecipient.getId().toString(), sum,
                    Status.Approved, cardDebitSender.getCurrency());
            historyOperationRepository.save(historyOperation);
        } else {
            System.out.println("Ошибка при переводе денежных средств: недостаточно средств на балансе");
        }

    }

    //Перевод со счёта на карту
    public void transferScoreToCard(long id, String numberCard, double sum) {
        Score scoreSender = scoreRepository.findById(id).get();
        String codeBank = "5456555";
        long idCard = Long.parseLong(numberCard.substring(7));
        String codeBankRecipient = numberCard.substring(0, 7);
        if (scoreSender.getBalance() >= sum) {
            if (codeBankRecipient.equals(codeBank)) {
                CardDebit cardDebitRecipient = cardDebitRepository.findById(idCard).get();
                scoreSender.setBalance(scoreSender.getBalance() - sum);
                cardDebitRecipient.setBalance(cardDebitRecipient.getBalance() + sum);
                scoreRepository.save(scoreSender);
                cardDebitRepository.save(cardDebitRecipient);
                HistoryOperation historyOperation = new HistoryOperation(scoreSender.getId().toString(),
                        numberCard, sum,
                        Status.Approved, scoreSender.getCurrency());
                historyOperationRepository.save(historyOperation);
            } else {
                scoreSender.setBalance(scoreSender.getBalance() - sum);
                scoreRepository.save(scoreSender);
                HistoryOperation historyOperation = new HistoryOperation(scoreSender.getId().toString(),
                        numberCard, sum,
                        Status.Waiting, scoreSender.getCurrency());
                historyOperationRepository.save(historyOperation);
            }
        } else {
            System.out.println("Ошибка при переводе денежных средств: недостаточно средств на балансе");
        }
    }

    //Список историй всех операций перевода
    public Iterable<HistoryOperation> historyOperations() {
        Iterable<HistoryOperation> historyOperations = historyOperationRepository.findAll();
        return historyOperations;
    }

    //Подробная информация о операции
    public ArrayList<HistoryOperation> infoOperation(Long id) {
        Optional<HistoryOperation> operation = historyOperationRepository.findById(id);
        ArrayList<HistoryOperation> res = new ArrayList<>();
        operation.ifPresent(res::add);
        return res;
    }

    //Удаление истории
    public void deleteHistoryOperation(long id) {
        historyOperationRepository.deleteById(id);
    }
}
