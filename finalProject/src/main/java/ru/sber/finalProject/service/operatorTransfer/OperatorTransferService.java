package ru.sber.finalProject.service.operatorTransfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.finalProject.entity.transfer.HistoryOperation;
import ru.sber.finalProject.enums.Status;
import ru.sber.finalProject.repository.transfer.HistoryOperationRepository;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class OperatorTransferService {

    private final HistoryOperationRepository historyOperationRepository;

    @Autowired
    public OperatorTransferService(HistoryOperationRepository historyOperationRepository) {
        this.historyOperationRepository = historyOperationRepository;
    }

    //Отображение всех операций клиента
    public Iterable<HistoryOperation> listHistoryOperation() {
        return historyOperationRepository.findAll();
    }

    //Подробная информация о операции
    public ArrayList<HistoryOperation> infoOperation(Long id) {
        Optional<HistoryOperation> operation = historyOperationRepository.findById(id);
        ArrayList<HistoryOperation> res = new ArrayList<>();
        operation.ifPresent(res::add);
        return res;
    }

    //Одобрение операции
    public void approvedOperation(long id) {
        HistoryOperation operation = historyOperationRepository.findById(id).get();
        operation.setStatus(Status.Approved);
        historyOperationRepository.save(operation);
    }

    //Блокировка операции
    public void blockOperation(long id) {
        HistoryOperation operation = historyOperationRepository.findById(id).get();
        operation.setStatus(Status.Rejected);
        historyOperationRepository.save(operation);
    }
}
