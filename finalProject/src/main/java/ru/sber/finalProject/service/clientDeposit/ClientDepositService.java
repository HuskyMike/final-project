package ru.sber.finalProject.service.clientDeposit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.finalProject.entity.deposit.Deposit;
import ru.sber.finalProject.repository.deposit.DepositRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

@Service
public class ClientDepositService {

    private final DepositRepository depositRepository;

    @Autowired
    public ClientDepositService(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    //Список всех вкладов
    public Iterable<Deposit> deposits() {
        return depositRepository.findAll();
    }

    //Создание вложения
    public void createDeposit(String name, double min, double max, Date duration,
                              boolean investing, boolean closable, boolean capitalization, boolean possibilityTake) {
        //процент задает оператор
        int percent = 1;
        if (investing == false) {
            percent++;
        }
        if (closable == false) {
            percent++;
        }
        if (capitalization == false) {
            percent++;
        }
        if (possibilityTake == false) {
            percent++;
        }
        System.out.println(percent);
        Deposit deposit = new Deposit(name, min, min, max, duration, percent,
                investing, closable, capitalization, possibilityTake);
        Date date = new Date();
        deposit.setDateBegin(date);
        depositRepository.save(deposit);
    }

    //Подробная информация о вложении
    public ArrayList<Deposit> infoDeposit(Long id) {
        Optional<Deposit> deposit = depositRepository.findById(id);
        ArrayList<Deposit> res = new ArrayList<>();
        deposit.ifPresent(res::add);
        return res;
    }

    //Пополнить счет вложения
    public void putOnBalance(long id, double sum) {
        Deposit deposit = depositRepository.findById(id).get();
        if (deposit.isInvesting()) {
            deposit.setBalance(deposit.getBalance() + sum);
            depositRepository.save(deposit);
        }
    }

    //Снять со счета вложения
    public void takeOnBalance(long id, double sum) {
        Deposit deposit = depositRepository.findById(id).get();
        if (deposit.isPossibilityTake()) {
            deposit.setBalance(deposit.getBalance() - sum);
            depositRepository.save(deposit);
        }
    }

    //Закрыть счёт
    public void closeDeposit(long id) {
        Deposit deposit = depositRepository.findById(id).get();
        if (deposit.isClosable()) {
            depositRepository.deleteById(id);
        }
    }

}
