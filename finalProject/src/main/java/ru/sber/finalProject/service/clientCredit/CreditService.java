package ru.sber.finalProject.service.clientCredit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sber.finalProject.entity.credit.Application;
import ru.sber.finalProject.entity.credit.Card;
import ru.sber.finalProject.entity.credit.Credit;
import ru.sber.finalProject.entity.credit.Offer;
import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.enums.Status;
import ru.sber.finalProject.enums.StatusCard;
import ru.sber.finalProject.repository.credit.ApplicationRepository;
import ru.sber.finalProject.repository.credit.CardRepository;
import ru.sber.finalProject.repository.credit.CreditRepository;
import ru.sber.finalProject.repository.credit.OfferRepository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

@Service
public class CreditService {

    private final CreditRepository creditRepository;
    private final ApplicationRepository applicationRepository;
    private final OfferRepository offerRepository;
    private final CardRepository cardRepository;

    @Autowired
    public CreditService(CreditRepository creditRepository, ApplicationRepository applicationRepository,
                         OfferRepository offerRepository, CardRepository cardRepository) {
        this.creditRepository = creditRepository;
        this.applicationRepository = applicationRepository;
        this.offerRepository = offerRepository;
        this.cardRepository = cardRepository;
    }

    //Сбор всех строк из таблицы кредита
    public Iterable<Credit> listCredit() {
        Iterable<Credit> credits = creditRepository.findAll();
        return credits;
    }

    //Заявок на кредит
    public Iterable<Application> listApplication() {
        Iterable<Application> applications = applicationRepository.findAll();
        return applications;
    }

    //Добавление заявки в базу данных
    public void enterApplication(String name, Currency currency) {
        Application application = new Application(name, currency, Status.Waiting);
        applicationRepository.save(application);
    }

    //Получение информации о заявки
    public ArrayList<Application> applicationId(Long id) {
        Optional<Application> application = applicationRepository.findById(id);
        ArrayList<Application> res = new ArrayList<>();
        application.ifPresent(res::add);
        return res;
    }

    //Удаления заявки клиентом
    public void removeApplication(long id) {
        Application application = applicationRepository.findById(id).get();
        applicationRepository.delete(application);
    }

    //Список предложений
    public Iterable<Offer> listOffer() {
        Iterable<Offer> offers = offerRepository.findAll();
        return offers;
    }

    //Получение одного предложения
    public ArrayList<Offer> offerId(Long id) {
        Optional<Offer> offer = offerRepository.findById(id);
        ArrayList<Offer> res = new ArrayList<>();
        offer.ifPresent(res::add);
        return res;
    }

    //Добавление карты клиенту
    public void addCard(Long id) {
        if (offerRepository.existsById(id)) {
            Optional<Offer> offerParameters = offerRepository.findById(id);
            Offer offer = offerParameters.get();
            int random = 0 + (int) (Math.random() * 100);
            String number = "5456" + random;
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, 356);
            Card card = new Card(number, 0, offer.getLimit(), calendar.getTime(),
                    StatusCard.Connect, offer.getPercent(), null);
            cardRepository.save(card);
        }
    }

    //Удаление предложения
    public void removeOffer(Long id) {
        Offer offer = offerRepository.findById(id).get();
        offerRepository.delete(offer);
    }

}
