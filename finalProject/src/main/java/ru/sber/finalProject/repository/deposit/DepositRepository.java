package ru.sber.finalProject.repository.deposit;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.finalProject.entity.deposit.Deposit;

public interface DepositRepository extends JpaRepository<Deposit, Long> {
}
