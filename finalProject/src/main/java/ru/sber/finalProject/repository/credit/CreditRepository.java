package ru.sber.finalProject.repository.credit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.finalProject.entity.credit.Credit;

@Repository
public interface CreditRepository extends JpaRepository<Credit, Long> {
}
