package ru.sber.finalProject.repository.credit;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sber.finalProject.entity.credit.Offer;

@Repository
public interface OfferRepository extends CrudRepository<Offer, Long> {
}
