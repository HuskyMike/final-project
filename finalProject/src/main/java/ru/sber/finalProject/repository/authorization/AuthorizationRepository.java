package ru.sber.finalProject.repository.authorization;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.finalProject.entity.security.Authorization;

public interface AuthorizationRepository extends JpaRepository<Authorization, Long> {
    Authorization getFirstByLogin(String login);
}
