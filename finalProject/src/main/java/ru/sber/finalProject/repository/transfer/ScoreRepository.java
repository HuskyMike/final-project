package ru.sber.finalProject.repository.transfer;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.finalProject.entity.transfer.Score;

public interface ScoreRepository extends JpaRepository<Score, Long> {
}
