package ru.sber.finalProject.repository.transfer;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.finalProject.entity.transfer.CardDebit;

public interface CardDebitRepository extends JpaRepository<CardDebit, Long> {
}
