package ru.sber.finalProject.repository.credit;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.sber.finalProject.entity.credit.Application;

@Repository
public interface ApplicationRepository extends CrudRepository<Application, Long> {
}
