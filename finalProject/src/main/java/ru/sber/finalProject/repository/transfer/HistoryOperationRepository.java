package ru.sber.finalProject.repository.transfer;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sber.finalProject.entity.transfer.HistoryOperation;

public interface HistoryOperationRepository extends JpaRepository<HistoryOperation, Long> {
}
