package ru.sber.finalProject.repository.credit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.finalProject.entity.credit.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, Long> {

}
