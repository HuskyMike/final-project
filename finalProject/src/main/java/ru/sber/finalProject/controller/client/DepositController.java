package ru.sber.finalProject.controller.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.finalProject.service.clientDeposit.ClientDepositService;

import java.util.Date;

@Controller
@RequestMapping("/client/deposit")
public class DepositController {

    private final ClientDepositService clientDepositService;

    @Autowired
    public DepositController(ClientDepositService clientDepositService) {
        this.clientDepositService = clientDepositService;
    }

    //главная страница
    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("deposits", clientDepositService.deposits());
        return "client/deposit/mainDeposits-client";
    }

    //информация о вложении
    @GetMapping("/info/{id}")
    public String infoDeposit(@PathVariable long id, Model model) {
        model.addAttribute("infoDeposit", clientDepositService.infoDeposit(id));
        return "client/deposit/infoDeposits-client";
    }

    //Пополнить баланс
    @PostMapping("/info/{id}")
    public String putOnBalance(@PathVariable long id, @RequestParam double sum) {
        clientDepositService.putOnBalance(id, sum);
        return "redirect:/client/deposit/info/" + id;
    }

    //Для снятия
    @GetMapping("/info/take/{id}")
    public String infoDepositTest(@PathVariable long id, Model model) {
        model.addAttribute("infoDeposit", clientDepositService.infoDeposit(id));
        return "client/deposit/infoDepositPut-client";
    }

    //Снять с баланса
    @PostMapping("/info/take/{id}")
    public String takeOnBalance(@PathVariable long id, @RequestParam double sum) {
        clientDepositService.takeOnBalance(id, sum);
        return "redirect:/client/deposit/info/take/" + id;
    }

    //Закрыть вложение
    @GetMapping("/info/close/{id}")
    public String closeDeposit(@PathVariable long id) {
        clientDepositService.closeDeposit(id);
        return "redirect:/client/deposit/";
    }

    //Открытие вложения GET
    @GetMapping("/create")
    public String getCreateDeposit() {
        return "client/deposit/createDeposit-client";
    }

    //Открытие вложения POST
    @PostMapping("/create")
    public String postCreateDeposit(@RequestParam String name, @RequestParam double min,
                                    @RequestParam double max,
                                    @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date duration,
                                    @RequestParam(defaultValue = "false") boolean investing,
                                    @RequestParam(defaultValue = "false") boolean closable,
                                    @RequestParam(defaultValue = "false") boolean capitalization,
                                    @RequestParam(defaultValue = "false") boolean possibilityTake) {
        clientDepositService.createDeposit(name, min, max, duration,
                investing, closable, capitalization, possibilityTake);
        return "redirect:/client/deposit/";
    }


}
