package ru.sber.finalProject.controller.operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.finalProject.service.operatorDeposit.OperatorDepositService;

import java.util.Date;

@Controller
@RequestMapping("/operator/deposit")
public class OperatorDepositController {

    private final OperatorDepositService operatorDepositService;

    @Autowired
    public OperatorDepositController(OperatorDepositService operatorDepositService) {
        this.operatorDepositService = operatorDepositService;
    }

    //Страница всех операций клиента
    @GetMapping("/")
    public String mainDeposit(Model model) {
        model.addAttribute("listDeposits", operatorDepositService.deposits());
        return "operator/deposits/mainDeposit-operator";
    }

    //Подробное отображение вклада
    @GetMapping("/info/{id}")
    public String infoDeposit(@PathVariable long id, Model model) {
        model.addAttribute("infoDeposit", operatorDepositService.infoDeposit(id));
        return "operator/deposits/infoDeposit-operator";
    }

    //Изменение параметров вложения GET
    @GetMapping("/info/{id}/edit")
    public String getUpdateDeposit(@PathVariable long id, Model model) {
        model.addAttribute("infoDeposit", operatorDepositService.infoDeposit(id));
        return "operator/deposits/editDeposit-operator";
    }

    //Изменение параметров вложения POST
    @PostMapping("/info/{id}/edit")
    public String postUpdateDeposit(@PathVariable long id, @RequestParam String name,
                                    @RequestParam double min, @RequestParam double max,
                                    @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date duration,
                                    @RequestParam double percent,
                                    @RequestParam(defaultValue = "false") boolean investing,
                                    @RequestParam(defaultValue = "false") boolean closable,
                                    @RequestParam(defaultValue = "false") boolean capitalization,
                                    @RequestParam(defaultValue = "false") boolean possibilityTake) {
        operatorDepositService.editDeposit(id, name, min, max, duration, percent,
                investing, closable, capitalization, possibilityTake);
        return "redirect:/operator/deposit/info/" + id;
    }
}
