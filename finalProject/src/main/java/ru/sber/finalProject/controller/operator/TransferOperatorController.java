package ru.sber.finalProject.controller.operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sber.finalProject.service.operatorTransfer.OperatorTransferService;

@Controller
@RequestMapping("/operator/transfer")
public class TransferOperatorController {

    private final OperatorTransferService operatorTransferService;

    @Autowired
    public TransferOperatorController(OperatorTransferService operatorTransferService) {
        this.operatorTransferService = operatorTransferService;
    }

    //Страница всех операций клиента
    @GetMapping("/")
    public String mainTransfer(Model model) {
        model.addAttribute("listHistory", operatorTransferService.listHistoryOperation());
        return "operator/transfer/historyOperation-operator";
    }

    //Детальная информация о операции
    @GetMapping("/info/{id}")
    public String infoOperation(@PathVariable long id, Model model) {
        model.addAttribute("infoOperation", operatorTransferService.infoOperation(id));
        return "operator/transfer/infoHistoryOperation-operator";
    }

    //Одобрение операции
    @GetMapping("/approved/{id}")
    public String approveOperation(@PathVariable long id) {
        operatorTransferService.approvedOperation(id);
        return "redirect:/operator/transfer/info/" + id;
    }

    //Заблокировать операцию
    @GetMapping("/rejected/{id}")
    public String rejectedOperation(@PathVariable long id) {
        operatorTransferService.blockOperation(id);
        return "redirect:/operator/transfer/info/" + id;
    }
}
