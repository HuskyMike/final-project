package ru.sber.finalProject.controller.operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.finalProject.service.operatorCredit.OperatorService;

@Controller
@RequestMapping("/operator")
public class OperatorController {

    private final OperatorService operatorService;

    @Autowired
    public OperatorController(OperatorService operatorService) {
        this.operatorService = operatorService;
    }

    //главная старница оператора
    @GetMapping("/")
    public String mainOperator(Model model) {
        model.addAttribute("credits", operatorService.listCredit());
        return "operator/main";
    }

    //Страница с информацией о кредите и изменении процента комиссии
    @GetMapping("/credit/{id}")
    public String infoCredit(@PathVariable long id, Model model) {
        model.addAttribute("infoCredit", operatorService.credit(id));
        return "operator/credit/updateCommission";
    }

    //Изменении процента комиссии
    @PostMapping("/credit/{id}")
    public String updateCommission(@PathVariable long id, @RequestParam double commission) {
        operatorService.updateCommission(id, commission);
        return "redirect:/operator/";
    }

    //Страница с информацией о кредите и изменении процента комиссии
    @GetMapping("/credit/penalties/{id}")
    public String infoCreditPenalties(@PathVariable long id, Model model) {
        model.addAttribute("infoCredit", operatorService.credit(id));
        return "operator/credit/updatePenalties-operator";
    }

    //Изменении процента комиссии
    @PostMapping("/credit/penalties/{id}")
    public String updatePenalties(@PathVariable long id, @RequestParam double penaltiesPercent) {
        operatorService.updatePenaltiesPercent(id, penaltiesPercent);
        return "redirect:/operator/";
    }


}
