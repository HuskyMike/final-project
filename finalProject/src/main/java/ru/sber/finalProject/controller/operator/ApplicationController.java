package ru.sber.finalProject.controller.operator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.finalProject.repository.credit.ApplicationRepository;
import ru.sber.finalProject.service.operatorCredit.OperatorService;

@Controller
@RequestMapping("/operator/application")
public class ApplicationController {

    private final OperatorService operatorService;

    @Autowired
    public ApplicationController(OperatorService operatorService, ApplicationRepository applicationRepository) {
        this.operatorService = operatorService;
    }

    //Странница с заявками клиентов
    @GetMapping("/")
    public String applications(Model model) {
        model.addAttribute("app", operatorService.applicationList());
        return "operator/credit/application-operator";
    }

    //Страница с предложениями
    @GetMapping("/offer")
    public String offers(Model model) {
        model.addAttribute("listOffer", operatorService.listOffer());
        return "operator/credit/offer-operator";
    }

    //Форма на обработку заявки
    @GetMapping("/info/{id}")
    public String infoApplication(@PathVariable long id, Model model) {
        model.addAttribute("infoApplication", operatorService.applicationId(id));
        return "operator/credit/applicationInfo";
    }

    //Создание обращения
    @PostMapping("/info/{id}")
    public String createOffer(@PathVariable long id, @RequestParam double limit,
                              @RequestParam double percent) {
        operatorService.createOffer(id, limit, percent);
        operatorService.removeApplication(id);
        return "redirect:/operator/application/offer";
    }
}
