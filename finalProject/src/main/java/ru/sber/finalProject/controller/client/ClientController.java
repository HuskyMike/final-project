package ru.sber.finalProject.controller.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.service.clientCredit.ClientService;

@Controller
@RequestMapping("/client")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    //главная старница клиента с картами
    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("listCards", clientService.listCards());
        return "client/main";
    }

    //информация о карте
    @GetMapping("/info/{id}")
    public String infoCard(@PathVariable("id") long id, Model model) {
        model.addAttribute("infoCard", clientService.infoCard(id));
        model.addAttribute("creditsCard", clientService.creditList(id));
        return "/client/card/infoCard";
    }

    //Страница для формирования кредита
    @GetMapping("/add/{id}")
    public String infoApplication(@PathVariable long id, Model model) {
        model.addAttribute("cards", clientService.card(id));
        return "/client/card/createCredit";
    }

    //Добавление кредита
    @PostMapping("/add/{id}")
    public String addCredit(@PathVariable long id, @RequestParam String name,
                            @RequestParam double sum, @RequestParam Currency currency) {
        clientService.createCredit(name, sum, currency, id);
        return "redirect:/client/info/{id}";
    }

    //Погашение кредита
    @GetMapping("/repayment/{id}")
    public String formRepayment(@PathVariable long id, Model model) {
        model.addAttribute("credit", clientService.credit(id));
        return "/client/card/repaymentCredit";
    }

    //Погашение кредита
    @PostMapping("/repayment/{id}")
    public String addRepayment(@PathVariable long id, @RequestParam double sum) {
        clientService.repaymentCredit(id, sum);
        return "redirect:/client/";
    }

}
