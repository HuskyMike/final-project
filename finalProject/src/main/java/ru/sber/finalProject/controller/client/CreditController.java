package ru.sber.finalProject.controller.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.repository.credit.ApplicationRepository;
import ru.sber.finalProject.service.clientCredit.CreditService;

@Controller
@RequestMapping("/credit")
public class CreditController {

    private final CreditService creditService;

    @Autowired
    public CreditController(CreditService creditService, ApplicationRepository applicationRepository) {
        this.creditService = creditService;
    }

    //главная страница
    @GetMapping("/")
    public String credit(Model model) {
        model.addAttribute("listCredit", creditService.listCredit());
        return "client/credit/main-credit";
    }

    //Открыть страницу добвления заявки
    @GetMapping("/add")
    public String add() {
        return "client/credit/add-credit";
    }

    //Страница с заявками
    @GetMapping("/application")
    public String application(Model model) {
        model.addAttribute("listApplication", creditService.listApplication());
        return "client/credit/application-credit";
    }

    //Добавить заявку
    @PostMapping("/add")
    public String addApplication(@RequestParam String nameApplication, @RequestParam Currency currency) {
        creditService.enterApplication(nameApplication, currency);
        return "redirect:/credit/application";
    }

    //Детальная информация о заявки
    @GetMapping("/application/{id}")
    public String applicationId(@PathVariable("id") long id, Model model) {
        model.addAttribute("applicationId", creditService.applicationId(id));
        return "client/credit/applicationId-credit";
    }

    //Удаление заяки
    @GetMapping("/application/{id}/remove")
    public String removeApplication(@PathVariable("id") long id) {
        creditService.removeApplication(id);
        return "redirect:/credit/application";
    }

    //Страница с предложениями
    @GetMapping("/offer")
    public String offer(Model model) {
        model.addAttribute("listOffer", creditService.listOffer());
        return "client/credit/offer-credit";
    }

    //Информация о предложении
    @GetMapping("/offer/{id}")
    public String OfferDetails(@PathVariable("id") long id, Model model) {
        model.addAttribute("offer", creditService.offerId(id));
        return "client/credit/offerId-credit";
    }

    //Создание карты
    @GetMapping("/offer/{id}/create")
    public String createCard(@PathVariable("id") long id) {
        creditService.addCard(id);
        creditService.removeOffer(id);
        return "redirect:/client/";
    }

    //Отклонить предложение
    @GetMapping("/offer/{id}/remove")
    public String deleteOffer(@PathVariable("id") long id) {
        creditService.removeOffer(id);
        return "redirect:/credit/offer";
    }

}
