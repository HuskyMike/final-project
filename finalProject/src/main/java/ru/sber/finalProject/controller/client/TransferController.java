package ru.sber.finalProject.controller.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sber.finalProject.enums.Currency;
import ru.sber.finalProject.service.clientTransfer.ClientTransferService;

@Controller
@RequestMapping("/client/transfer")
public class TransferController {

    private final ClientTransferService clientTransferService;

    @Autowired
    public TransferController(ClientTransferService clientTransferService) {
        this.clientTransferService = clientTransferService;
    }


    //главная старница клиента "переводы"
    @GetMapping("/")
    public String mainTransfer() {
        return "client/transfer/main-transfer";
    }

    //страница с картами
    @GetMapping("/card")
    public String cardsDebit(Model model) {
        model.addAttribute("cardsDebit", clientTransferService.cardsDebit());
        return "client/transfer/card-transfer";
    }

    //страница с информацией о дебитовой карте
    @GetMapping("/card/info/{id}")
    public String infoCardDebit(@PathVariable long id, Model model) {
        model.addAttribute("infoCardDebit", clientTransferService.infoCardDebit(id));
        return "client/transfer/infoCardDebit-transfer";
    }

    //Удаление дебетовой карты
    @GetMapping("/card/remove/{id}")
    public String infoCardDebit(@PathVariable long id) {
        clientTransferService.deleteCardDebit(id);
        return "redirect:/client/transfer/card";
    }

    //Страница создания дебетовой карты
    @GetMapping("/card/create")
    public String getCardDebit() {
        return "client/transfer/createCardDebit-transfer";
    }

    //Заполнение данных для создания дебетовой карты
    @PostMapping("/card/create")
    public String postCardDebit(@RequestParam Currency currency) {
        clientTransferService.createCardDebit(currency);
        return "redirect:/client/transfer/card";
    }

    //Перевод с карты на карту GET
    @GetMapping("/card/transferToCard/{id}")
    public String getCardTransferToCard(@PathVariable long id, Model model) {
        model.addAttribute("infoCard", clientTransferService.infoCardDebit(id));
        return "client/transfer/transferCardToCard-transfer";
    }

    //Перевод с карты на карту POST
    @PostMapping("/card/transferToCard/{id}")
    public String postCardTransferToCard(@PathVariable long id, @RequestParam String numberCard,
                                         @RequestParam double sum) {
        clientTransferService.transferCardToCard(id, numberCard, sum);
        return "redirect:/client/transfer/card";
    }

    //Перевод с карты на счет GET
    @GetMapping("/card/transferCardToScore/{id}")
    public String getCardTransferToScore(@PathVariable long id, Model model) {
        model.addAttribute("infoCard", clientTransferService.infoCardDebit(id));
        return "client/transfer/transferCardToScore-transfer";
    }

    //Перевод с карты на счет POST
    @PostMapping("/card/transferCardToScore/{id}")
    public String postCardTransferToScore(@PathVariable long id, @RequestParam long numberCard,
                                          @RequestParam double sum) {
        clientTransferService.transferCardToScore(id, numberCard, sum);
        return "redirect:/client/transfer/card";
    }

    //страница со счетами
    @GetMapping("/score")
    public String scoresTransfer(Model model) {
        model.addAttribute("scoreList", clientTransferService.scores());
        return "client/transfer/score-transfer";
    }

    //Страница создания счета
    @GetMapping("/score/create")
    public String getCreateScore() {
        return "client/transfer/createScore-transfer";
    }

    //страница со счетами
    @PostMapping("/score/create")
    public String postCreateScore(@RequestParam double balance, @RequestParam Currency currency) {
        clientTransferService.createScore(balance, currency);
        return "redirect:/client/transfer/score";
    }


    //Страница перевода денежных средств на другой счет
    @GetMapping("/score/transferScore/{id}")
    public String getTransferScore(@PathVariable long id, Model model) {
        model.addAttribute("infoScore", clientTransferService.infoScore(id));
        return "client/transfer/transferMoneyToScore-transfer";
    }

    //Заполнение данных для перевода и перевод на другой счёт
    @PostMapping("/score/transferScore/{id}")
    public String postTransferScore(@PathVariable long id, @RequestParam long idScore,
                                    @RequestParam double sum) {
        clientTransferService.transferMoneyToScore(id, idScore, sum);
        return "redirect:/client/transfer/score";
    }

    //Перевод со счета на карту GET
    @GetMapping("/score/transferScoreToCard/{id}")
    public String getScoreTransferToCard(@PathVariable long id, Model model) {
        model.addAttribute("infoScore", clientTransferService.infoScore(id));
        return "client/transfer/transferScoreToCard-transfer";
    }

    //Перевод со счета на карту POST
    @PostMapping("/score/transferScoreToCard/{id}")
    public String postScoreTransferToCard(@PathVariable long id, @RequestParam String numberCard,
                                          @RequestParam double sum) {
        clientTransferService.transferScoreToCard(id, numberCard, sum);
        return "redirect:/client/transfer/score";
    }

    //Удаление счета
    @GetMapping("/score/delete/{id}")
    public String deleteScore(@PathVariable long id) {
        clientTransferService.deleteScore(id);
        return "redirect:/client/transfer/score";
    }

    //История операций
    @GetMapping("/history")
    public String historyOperation(Model model) {
        model.addAttribute("historyOperations", clientTransferService.historyOperations());
        return "client/transfer/historyOperation-transfer";
    }

    //Подробная информация об операции
    @GetMapping("/history/info/{id}")
    public String infoHistoryOperation(@PathVariable long id, Model model) {
        model.addAttribute("infoOperation", clientTransferService.infoOperation(id));
        return "client/transfer/infoHistoryClient-client";
    }

    //Удаление операции из истории
    @GetMapping("/history/delete/{id}")
    public String deleteHistoryOperation(@PathVariable long id) {
        clientTransferService.deleteHistoryOperation(id);
        return "redirect:/client/transfer/history";
    }

}
